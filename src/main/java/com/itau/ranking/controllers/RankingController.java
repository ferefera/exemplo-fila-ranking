package com.itau.ranking.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;

@RestController
public class RankingController {

	@Autowired
	private RankingRepository rankingRepository;

	@Autowired
	private JmsTemplate jmsTemplate;

	@RequestMapping(method = RequestMethod.POST, path = "/incluirRanking")
	public Ranking incluirRanking(@RequestBody Ranking ranking) {

		double perguntasCertas = ranking.getHits();
		double perguntasErradas = ranking.getMisses();

		double totalPerguntas = perguntasCertas + perguntasErradas;

		double porcentagemAcerto = (perguntasCertas / totalPerguntas);

		double pontos = porcentagemAcerto * 100;

		ranking.setTotal((int) pontos);

		HashMap<String, String> body = new HashMap<>();

		body.put("playerName", ranking.getPlayerName());
		body.put("gameId", ranking.getGameId());
		body.put("hits", ranking.getHits() + "");
		body.put("misses", ranking.getMisses() + "");
		body.put("total", ranking.getTotal() + "");

		jmsTemplate.convertAndSend("b.queue.ranking", body);

		return rankingRepository.save(ranking);

	}

	@RequestMapping(method = RequestMethod.GET, path = "/consultarRanking")
	public ResponseEntity<ArrayList<Ranking>> consultarRanking() {

		ArrayList<Ranking> rankings = rankingRepository.findTop10ByOrderByTotalDesc();

		if (rankings.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok().body(rankings);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/consultarRanking/{nomeJogo}")
	public ResponseEntity<ArrayList<Ranking>> consultarRanking(@PathVariable String gameId) {

		ArrayList<Ranking> rankings = rankingRepository.findTop10ByGameIdOrderByTotalDesc(gameId);

		if (rankings.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok().body(rankings);
		}

	}

}