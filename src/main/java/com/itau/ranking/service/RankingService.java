package com.itau.ranking.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;

@Component
public class RankingService {
	
	@Autowired
	private RankingRepository rankingRepository;

	@JmsListener(destination = "b.queue.ranking")
	public void receiveMessage(HashMap<String, String> msg) {
		System.out.println("Mensagem Recebida - " + msg.get("playerName"));
		
		Ranking ranking = new Ranking();
		
		ranking.setPlayerName(msg.get("playerName"));
		ranking.setGameId(msg.get("gameId"));
		ranking.setHits(Integer.parseInt(msg.get("hits")));
		ranking.setMisses(Integer.parseInt(msg.get("misses")));
		
		double perguntasCertas = ranking.getHits();
		double perguntasErradas = ranking.getMisses();
		
		double totalPerguntas = perguntasCertas + perguntasErradas;
		
		double porcentagemAcerto = (perguntasCertas / totalPerguntas);
		
		double pontos = porcentagemAcerto * 100;
		
		
		ranking.setTotal((int) pontos);
		
		rankingRepository.save(ranking);
		
		
	}
}
