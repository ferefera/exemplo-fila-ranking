package com.itau.ranking.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.itau.ranking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long> {

	public ArrayList<Ranking> findTop10ByOrderByTotalDesc();
	
	public ArrayList<Ranking> findTop10ByGameIdOrderByTotalDesc(String gameId);
	
}
